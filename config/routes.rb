Rails.application.routes.draw do
  resources :mypokemons
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  root 'pokemon#index'
  get '/(:page)', to: 'pokemon#index'
  post 'pokemon/index/(:name)', to: 'pokemon#index'
  get 'pokemon/show/(:name)', to: 'pokemon#show'
  # get 'favorites/index', to: 'favorites#index'
  # post 'favorites/create', to: 'favorites#create'
end
