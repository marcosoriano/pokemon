json.extract! mypokemon, :id, :name, :nickname, :image, :created_at, :updated_at
json.url mypokemon_url(mypokemon, format: :json)
