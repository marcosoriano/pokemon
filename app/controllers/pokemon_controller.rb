class PokemonController < ApplicationController
    require 'poke-api-v2'
    def index

        limit = 15 #initializing limit
        page = params.has_key?(:page) ? params[:page].to_i : 1 #get the currrennt page
        offset = (page - 1) * limit #determine the offset of data

        if(params.has_key?(:name) && !params[:name].blank?) #check wether the parameter name is set or not balnk
            @name = params[:name]   
            begin
                pokemon_list = PokeApi.get(pokemon: params[:name].downcase) #display the searched pokemon
            rescue => e
                @message = "No Results Found on #{params[:name]}" #display message when pokemon name is not in the data
            end
            @pokemons = pokemon_list 
        else
            pokemon_list = PokeApi.get(pokemon: {limit: limit, offset: offset}) #display all 15 pokemon

            pokemons_with_images = pokemon_list.results.map do |pokemon| #map the pokemon sprites
                details = PokeApi.get(pokemon: pokemon.name)
                {
                  name: pokemon.name,
                  image: details.sprites.front_default
                }
            end
    
            @pokemons = pokemons_with_images
        end
       
        @page = page
        # @pokemons.paginate
        # @pokemons = pokemons_with_images.paginate(page: page, per_page: limit, total_entries: 25)

    end

    def show
        pokemon = PokeApi.get(pokemon: params[:name].downcase)
        @pokemon = pokemon
    end
end
