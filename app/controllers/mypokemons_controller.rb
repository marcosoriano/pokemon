class MypokemonsController < ApplicationController
  before_action :set_mypokemon, only: %i[ show edit update destroy ]

  # GET /mypokemons or /mypokemons.json
  def index
    @mypokemons = Mypokemon.where(email: current_user.email)
    # @mypokemons = Mypokemon.all
  end

  # GET /mypokemons/1 or /mypokemons/1.json
  def show
  end

  # GET /mypokemons/new
  def new
    @mypokemon = Mypokemon.new
  end

  # GET /mypokemons/1/edit
  def edit
  end

  # POST /mypokemons or /mypokemons.json
  def create
    @mypokemon = Mypokemon.new(mypokemon_params)

    respond_to do |format|
      if @mypokemon.save
        format.html { redirect_to mypokemon_url(@mypokemon), notice: "Mypokemon was successfully created." }
        format.json { render :show, status: :created, location: @mypokemon }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @mypokemon.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mypokemons/1 or /mypokemons/1.json
  def update
    respond_to do |format|
      if @mypokemon.update(mypokemon_params)
        format.html { redirect_to mypokemon_url(@mypokemon), notice: "Mypokemon was successfully updated." }
        format.json { render :show, status: :ok, location: @mypokemon }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @mypokemon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mypokemons/1 or /mypokemons/1.json
  def destroy
    @mypokemon.destroy

    respond_to do |format|
      format.html { redirect_to mypokemons_url, notice: "Mypokemon was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mypokemon
      @mypokemon = Mypokemon.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def mypokemon_params
      params.require(:mypokemon).permit(:name, :nickname, :image, :email)
    end
end
