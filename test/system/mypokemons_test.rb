require "application_system_test_case"

class MypokemonsTest < ApplicationSystemTestCase
  setup do
    @mypokemon = mypokemons(:one)
  end

  test "visiting the index" do
    visit mypokemons_url
    assert_selector "h1", text: "Mypokemons"
  end

  test "should create mypokemon" do
    visit mypokemons_url
    click_on "New mypokemon"

    fill_in "Image", with: @mypokemon.image
    fill_in "Name", with: @mypokemon.name
    fill_in "Nickname", with: @mypokemon.nickname
    click_on "Create Mypokemon"

    assert_text "Mypokemon was successfully created"
    click_on "Back"
  end

  test "should update Mypokemon" do
    visit mypokemon_url(@mypokemon)
    click_on "Edit this mypokemon", match: :first

    fill_in "Image", with: @mypokemon.image
    fill_in "Name", with: @mypokemon.name
    fill_in "Nickname", with: @mypokemon.nickname
    click_on "Update Mypokemon"

    assert_text "Mypokemon was successfully updated"
    click_on "Back"
  end

  test "should destroy Mypokemon" do
    visit mypokemon_url(@mypokemon)
    click_on "Destroy this mypokemon", match: :first

    assert_text "Mypokemon was successfully destroyed"
  end
end
