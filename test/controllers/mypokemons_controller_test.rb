require "test_helper"

class MypokemonsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mypokemon = mypokemons(:one)
  end

  test "should get index" do
    get mypokemons_url
    assert_response :success
  end

  test "should get new" do
    get new_mypokemon_url
    assert_response :success
  end

  test "should create mypokemon" do
    assert_difference("Mypokemon.count") do
      post mypokemons_url, params: { mypokemon: { image: @mypokemon.image, name: @mypokemon.name, nickname: @mypokemon.nickname } }
    end

    assert_redirected_to mypokemon_url(Mypokemon.last)
  end

  test "should show mypokemon" do
    get mypokemon_url(@mypokemon)
    assert_response :success
  end

  test "should get edit" do
    get edit_mypokemon_url(@mypokemon)
    assert_response :success
  end

  test "should update mypokemon" do
    patch mypokemon_url(@mypokemon), params: { mypokemon: { image: @mypokemon.image, name: @mypokemon.name, nickname: @mypokemon.nickname } }
    assert_redirected_to mypokemon_url(@mypokemon)
  end

  test "should destroy mypokemon" do
    assert_difference("Mypokemon.count", -1) do
      delete mypokemon_url(@mypokemon)
    end

    assert_redirected_to mypokemons_url
  end
end
