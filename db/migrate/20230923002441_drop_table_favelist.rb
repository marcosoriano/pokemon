class DropTableFavelist < ActiveRecord::Migration[7.0]
  def change
    drop_table :favelists
  end
end
