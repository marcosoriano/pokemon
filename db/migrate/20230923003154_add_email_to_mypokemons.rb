class AddEmailToMypokemons < ActiveRecord::Migration[7.0]
  def change
    add_column :mypokemons, :email, :string
  end
end
