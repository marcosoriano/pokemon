class CreateFavPokemon < ActiveRecord::Migration[7.0]
  def change
    create_table :fav_pokemons do |t|
      t.string :name
      t.string :nickname
      t.string :image
      t.boolean :favorite
      t.timestamps
    end
  end
end
class DropFavPokemonTable < ActiveRecord::Migration[7.0]
  def change
    drop_table :fav_pokemons
  end
end