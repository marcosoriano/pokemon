class DropFavPokemonsTable < ActiveRecord::Migration[7.0]
  def change
    drop_table :fav_pokemons
  end
end
