class CreateMypokemons < ActiveRecord::Migration[7.0]
  def change
    create_table :mypokemons do |t|
      t.string :name
      t.string :nickname
      t.string :image

      t.timestamps
    end
  end
end
