class CreateFavelists < ActiveRecord::Migration[7.0]
  def change
    create_table :favelists do |t|
      t.string :name
      t.string :nickname
      t.string :image

      t.timestamps
    end
  end
end
