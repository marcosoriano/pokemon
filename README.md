
# Pokemon App

Pokemon app is a ruby on rails application that uses poke_api_v2 api for retrieving pokemon details. It allows to create a user that can add pokemon to their favorite list. List of pokemons are retrieved using the api. Users are allowed to view the more details information of the specific pokemon. The users can change name, nickname of the pokemons that are included on their favorite list. Users can also create their own pokemon. Users are also allowed to delete the pokemons on their favorite list.




## Requirements

Ruby >= 2.0.0
Poke_api_v2 installed >> instructions are found at https://github.com/rdavid1099/poke-api-v2/tree/main

## Installation

Clone the "Pokemon App" repository 
(https://gitlab.com/marcosoriano/pokemon.git)

Go to the repository by using the shell command
cd repository_name

Run database migration using the shell command
rake db:migrate

Start the rails server using the shell command
rails s


Enjoy!!!!

    